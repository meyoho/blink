FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.10

WORKDIR /blink

RUN mkdir -p /blink/dist/static

COPY . /blink/dist

ARG commit_id=dev
ARG app_version=dev
ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
