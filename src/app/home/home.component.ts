import {
  API_GATEWAY,
  AuthorizationStateService,
  KubernetesResourceList,
  StringMap,
  TranslateKey,
  ifExist,
  publishRef,
} from '@alauda/common-snippet'
import { HttpClient } from '@angular/common/http'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { safeLoad } from 'js-yaml'
import { map, switchMap } from 'rxjs/operators'
import { Production, ProductionGroup, ProductionSpec } from 'types'

export interface CardSection {
  title: TranslateKey
  items: ProductionSpec[]
  group?: ProductionGroup
}

@Component({
  selector: 'ab-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  fallbackLogo = 'logos/placeholder.svg'

  token$ = this.authState.getToken()

  cardSections$ = this.http
    .get('custom/layout.yaml', { responseType: 'text' })
    .pipe(
      switchMap(data =>
        this.http
          .get<KubernetesResourceList<Production>>(
            API_GATEWAY + '/apis/ace3.alauda.io/v1/productions',
          )
          .pipe(
            map(({ items }) => {
              const Groups: Array<{
                name: string
                title: string
                type: ProductionGroup
              }> = safeLoad(safeLoad(data).Groups)
              const groupTitles = Groups.reduce<StringMap>(
                (acc, { name, title }) =>
                  Object.assign(acc, {
                    [name]: title,
                  }),
                {},
              )

              return items.reduce<
                Partial<Record<ProductionGroup, CardSection>>
              >((cardSectionGroup, { spec }) => {
                const { group } = spec
                const title = groupTitles[group]
                if (!title) {
                  return cardSectionGroup
                }
                if (!cardSectionGroup[group]) {
                  cardSectionGroup[group] = {
                    title,
                    items: [],
                    group,
                  }
                }
                cardSectionGroup[group].items.push(spec)
                return cardSectionGroup
              }, {})
            }),
          ),
      ),
      map(cardSectionGroup =>
        Object.values(cardSectionGroup).map(cardSection => {
          cardSection.items = cardSection.items
            .slice()
            .sort((x, y) => x.index - y.index)
          return cardSection
        }),
      ),
      publishRef(),
    )

  constructor(
    private readonly http: HttpClient,
    private readonly authState: AuthorizationStateService,
  ) {}

  normalizeProdHomepage(homepage: string, token?: string) {
    return `${homepage}${ifExist<boolean | string>(
      !homepage.includes('?'),
      '?',
    )}id_token=${token}`
  }
}
