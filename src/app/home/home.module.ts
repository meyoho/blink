import { NgModule } from '@angular/core'
import { SharedModule } from 'shared/shared.module'

import { HomeComponent } from './home.component'

const EXPORTABLE = [HomeComponent]

@NgModule({
  imports: [SharedModule],
  declarations: EXPORTABLE,
  exports: EXPORTABLE,
})
export class HomeModule {}
