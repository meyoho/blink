import { Translation } from '@alauda/common-snippet'

export const zh: Translation = {
  console: ' 控制台',
  product: '产品',
  enterProduct: '进入产品',
  codeTool: '代码工具',
  devops: '运维管理',
  monitoring: '监控',
  log: '日志',
}
