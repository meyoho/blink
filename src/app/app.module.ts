import {
  ApiGatewayInterceptor,
  AuthorizationInterceptorService,
  Locale,
  TOKEN_RESOURCE_DEFINITIONS,
  TranslateModule,
} from '@alauda/common-snippet'
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RESOURCE_DEFINITIONS } from 'utils'

import { AppComponent } from './app.component'
import { HomeModule } from './home/home.module'
import { zh } from './i18n.zh'

@NgModule({
  imports: [
    BrowserModule,
    TranslateModule.forRoot({
      locales: [Locale.ZH],
      loose: true,
      translations: { zh },
    }),
    HomeModule,
  ],
  declarations: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
    {
      provide: TOKEN_RESOURCE_DEFINITIONS,
      useValue: RESOURCE_DEFINITIONS,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
