import { TranslateModule, UtilsModule } from '@alauda/common-snippet'
import {
  ButtonModule,
  CardModule,
  DropdownModule,
  IconModule,
  PageModule,
  SelectModule,
} from '@alauda/ui'
import { CommonModule } from '@angular/common'
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { RxDirectivesModule } from '@rxts/ngrx'

import { ComponentsModule } from './components/components.module'

const NG_MODULES = [CommonModule, HttpClientModule]
const AUI_MODULES = [
  ButtonModule,
  CardModule,
  IconModule,
  DropdownModule,
  PageModule,
  SelectModule,
]
const COMMON_MODULES = [TranslateModule, UtilsModule]
const THIRD_PARTY_MODULES = [RxDirectivesModule]

const EXPORTABLE_MODULES = [
  ...NG_MODULES,
  ...AUI_MODULES,
  ...COMMON_MODULES,
  ...THIRD_PARTY_MODULES,
  ComponentsModule,
]

@NgModule({
  imports: EXPORTABLE_MODULES,
  exports: [...EXPORTABLE_MODULES],
})
export class SharedModule {}
