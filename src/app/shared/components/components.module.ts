import { TranslateModule, UtilsModule } from '@alauda/common-snippet'
import { DropdownModule, IconModule } from '@alauda/ui'
import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'

import { AccountMenuComponent } from './account-menu/account-menu.component'
import { CardSectionComponent } from './card-section/card-section.component'

const EXPORTABLE = [AccountMenuComponent, CardSectionComponent]

@NgModule({
  imports: [
    CommonModule,
    DropdownModule,
    IconModule,
    TranslateModule,
    UtilsModule,
  ],
  exports: EXPORTABLE,
  declarations: EXPORTABLE,
})
export class ComponentsModule {}
