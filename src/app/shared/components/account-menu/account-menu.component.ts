import {
  AuthorizationStateService,
  K8sPermissionService,
  publishRef,
} from '@alauda/common-snippet'
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core'
import { Observable, Subject } from 'rxjs'
import { map } from 'rxjs/operators'
import { AccountInfo, Environments } from 'types'
import { ENVIRONMENTS, RESOURCE_TYPES } from 'utils'

export interface MenuDefinition {
  name: string
  icon: string
  link?: string
  type?: string
  href?: string
  allowed?: boolean
  env?: string
}

export const MENU_DEFINITIONS: MenuDefinition[] = [
  {
    name: 'project_management',
    icon: 'project_management_s',
    link: 'home/project',
    type: 'projectview',
  },
  {
    name: 'cluster_management',
    icon: 'server_management_s',
    href: '/console-cluster',
    type: 'clusterview',
    env: 'SYNC_TKE',
  },
  {
    name: 'platform_management',
    icon: 'setting_home',
    link: 'manage',
    type: 'platformview',
  },
  {
    name: 'my_profile',
    icon: 'user_s',
    link: 'home/personal-info',
    allowed: true,
  },
]

const CHECK_PERMISSION_TYPES = MENU_DEFINITIONS.reduce<string[]>(
  (acc, { allowed, type }) => {
    if (!allowed) {
      acc.push(type)
    }
    return acc
  },
  [],
)

@Component({
  selector: 'ab-account-menu',
  templateUrl: 'account-menu.component.html',
  styleUrls: ['account-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountMenuComponent {
  accountName$ = this.authState
    .getTokenPayload<AccountInfo>()
    .pipe(map(account => account.email || account.name))

  isMenusActivated$$ = new Subject<boolean>()

  accountMenus$: Observable<MenuDefinition[]> = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.VIEW,
      name: CHECK_PERMISSION_TYPES,
    })
    .pipe(
      map(statuses =>
        MENU_DEFINITIONS.map((menu, index) => ({
          ...menu,
          allowed: menu.allowed || statuses[index],
        })),
      ),
      publishRef(),
    )

  constructor(
    public authState: AuthorizationStateService,
    private readonly k8sPermission: K8sPermissionService,
    @Optional()
    @Inject(ENVIRONMENTS)
    public env: Environments,
  ) {}

  checkEnv(envs: Environments, key: keyof Environments) {
    if (!key) {
      return true
    }
    if (!envs) {
      return false
    }
    return envs[key] === 'true'
  }
}
