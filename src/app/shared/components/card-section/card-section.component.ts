import { ObservableInput } from '@alauda/common-snippet'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { Observable, combineLatest, fromEvent } from 'rxjs'
import { debounceTime, map, startWith } from 'rxjs/operators'

const MIN_CARD_WIDTH = 225
const MAX_CARD_WIDTH = 264
const CARD_MARGIN = 10
const SECTION_MARGIN = 20 * 2 - CARD_MARGIN

@Component({
  selector: 'ab-card-section',
  templateUrl: 'card-section.component.html',
  styleUrls: ['card-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardSectionComponent {
  @Input()
  title: string

  @ObservableInput(true, -1)
  @Input('count')
  count$: Observable<number>

  placeholders$ = combineLatest([
    this.count$,
    fromEvent(window, 'resize').pipe(
      debounceTime(500),
      // tslint:disable-next-line: deprecation
      startWith(null),
    ),
  ]).pipe(
    map(([count]) => {
      const containerWidth = document.body.clientWidth - SECTION_MARGIN
      return count < containerWidth / (MAX_CARD_WIDTH + CARD_MARGIN)
        ? null
        : Array.from({
            length: containerWidth / (MIN_CARD_WIDTH + CARD_MARGIN) - 1,
          })
    }),
  )
}
