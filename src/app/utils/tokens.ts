import { InjectionToken } from '@angular/core'
import { Environments } from 'types'

export const ENVIRONMENTS = new InjectionToken<Environments>(
  'dynamic environments from server',
)
