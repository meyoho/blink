import { TOKEN_BASE_HREF } from '@alauda/common-snippet'
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core'

@Component({
  selector: 'ab-app',
  template: '<ab-home></ab-home>',
  styles: [
    `
      :host {
        width: 100vw;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  constructor(@Inject(TOKEN_BASE_HREF) baseHref: string) {
    // we're not using Angular Router here because it is a so simple project with only one page
    if (location.pathname + location.search + location.hash !== baseHref) {
      history.replaceState(null, null, baseHref)
    }
  }
}
