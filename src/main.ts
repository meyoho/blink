import { recordInitUrl } from '@alauda/common-snippet'
import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'
import 'zone.js/dist/zone'

import { AppModule } from './app/app.module'
import { environment } from './environments/environment'

if (environment.production) {
  enableProdMode()
}

recordInitUrl()

// eslint-disable-next-line @typescript-eslint/no-floating-promises
platformBrowserDynamic()
  .bootstrapModule(AppModule)
  // eslint-disable-next-line @typescript-eslint/unbound-method
  .catch(console.error)
